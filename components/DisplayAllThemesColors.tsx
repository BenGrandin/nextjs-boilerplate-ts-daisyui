import Themes from '@/themes'

const DisplayAllThemesColors = () => {
	return (
		<section className='w-full flex justify-around'>
			{ (Object.entries(Themes).map(([ themeName, themeColors ]) => {
				const bgColor = themeColors['base-100'] ? '' : 'bg-gray-200'

				return (
					<div key={ themeName }
						 className={ `${ themeName } rounded p-4 ${ bgColor }` }
						 style={ {
							 backgroundColor: themeColors['base-100'],
							 border: `1px solid ${ themeColors['neutral'] }`,
						 } }>
						<h3 className='text-xl text-black'>{ themeName }</h3>
						{ Object.entries(themeColors).map(([ colorName, colorValue ]) => (
							<div key={ colorValue } style={ { backgroundColor: colorValue } }
								 className='p-1 m-1'>{ colorName + ': ' + colorValue }</div>
						)) }
					</div>
				)
			})) }
		</section>
	)
}
export default DisplayAllThemesColors
