import React, { useContext, useState } from 'react'
import Themes, { DEFAULT_DAISYUI_THEMES } from '@/themes'
import { ThemeContext } from '@/app/themeContext'

const ThemesDropdown: React.FC = () => {
	const customThemes = [ ...Object.keys(Themes) ]
	const [ isRounded, setIsRounded ] = useState(false)
	const rounded = isRounded ? 'rounded-t-box rounded-b-none' : 'rounded-box'

	function onClick() {
		setIsRounded(r => !r)
	}

	const { theme, setTheme } = useContext(ThemeContext);

	function onThemeClick(e: React.MouseEvent<HTMLElement>) {
		e.preventDefault()
		const { currentTarget: { textContent } } = e
		if (textContent) setTheme(textContent)
	}

	return (
		<div className='dropdown dropdown-end'>
			<label tabIndex={ 0 }
				   className='dropdown-label btn flex flex-row justify-center items-center focus:bg-neutral'>
				<svg
					width='18'
					height='18'
					xmlns='http://www.w3.org/2000/svg'
					fill='none'
					viewBox='0 0 24 24'
					className='inline-block h-5 w-5 stroke-current md:h-6 md:w-6'
				>
					<path
						strokeLinecap='round'
						strokeLinejoin='round'
						strokeWidth='2'
						d='M7 21a4 4 0 01-4-4V5a2 2 0 012-2h4a2 2 0 012 2v12a4 4 0 01-4 4zm0 0h12a2 2 0 002-2v-4a2 2 0 00-2-2h-2.343M11 7.343l1.657-1.657a2 2 0 012.828 0l2.829 2.829a2 2 0 010 2.828l-8.486 8.485M7 17h.01'
					></path>
				</svg>
				<span>Themes</span>
			</label>
			<ul tabIndex={ 0 }
				className={ `shadow menu dropdown-content z-[1] mt-2 p-0  bg-base-200 w-52 ${ rounded }` }>

				{ customThemes.sort().map(customTheme => {
					const ifTextPrimary = theme === customTheme ? 'text-primary' : ''

					return (
						<li onClick={ onThemeClick } key={ customTheme } className={ `p-2 ${ ifTextPrimary }` }>
							<a>{ customTheme }</a>
						</li>
					)
				}) }

				<details className={ `dropdown w-full bg-base-300 ${ rounded }` }>
					<summary className={ `btn w-full bg-neutral ${ rounded } text-info` } onClick={ onClick }>
						Default themes
					</summary>
					<ul className='dropdown-content z-[1]  rounded-b-box w-52 max-h-80 overflow-y-scroll bg-base-200'>
						{ DEFAULT_DAISYUI_THEMES.sort().map(defaultTheme => {
							const ifTextPrimary = theme === defaultTheme ? 'text-primary' : ''
							return (
								<li onClick={ onThemeClick } key={ defaultTheme } className={ ifTextPrimary }>
									<a>{ defaultTheme }</a>
								</li>
							)
						}) }
					</ul>
				</details>
			</ul>
		</div>
	)
}

export default ThemesDropdown
