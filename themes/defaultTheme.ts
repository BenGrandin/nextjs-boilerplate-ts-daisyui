const defaultTheme = {
	primary: "#570df8",

	secondary: "#f000b8",

	accent: "#37cdbe",

	neutral: "#191D24",

	"base-100": "#ffffff",
	"base-200": "#f9fafb",
	"base-300": "#ced3d9",

	info: "#3ABFF8",

	success: "#36D399",

	warning: "#FBBD23",

	error: "#F87272",





	"primary-focus": "#42b2b8",

	"secondary-focus": "#e7739e",

	"accent-focus": "#e09915",

	"neutral-focus": "#200f29",

};
export default defaultTheme
