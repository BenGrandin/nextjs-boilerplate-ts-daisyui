import cupcakeTheme from './cupcake'
import luxuryTheme from './luxury'
import defaultTheme from './defaultTheme'
import darkTheme from './darkTheme'

export const DEFAULT_DAISYUI_THEMES = [
	"light",
	"dark",
	"cupcake",
	"bumblebee",
	"emerald",
	"corporate",
	"synthwave",
	"retro",
	"cyberpunk",
	"valentine",
	"halloween",
	"garden",
	"forest",
	"aqua",
	"lofi",
	"pastel",
	"fantasy",
	"wireframe",
	"black",
	"luxury",
	"dracula",
	"cmyk",
	"autumn",
	"business",
	"acid",
	"lemonade",
	"night",
	"coffee",
	"winter",
]

const themes = {
	default1: defaultTheme,
	dark1: darkTheme,
	luxury: luxuryTheme,
	cupcake: cupcakeTheme,
};

export default themes
