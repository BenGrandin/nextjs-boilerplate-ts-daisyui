import type { Config } from 'tailwindcss'
import themes, { DEFAULT_DAISYUI_THEMES } from './themes/index'

const config: Config = {
	content: [
		'./pages/**/*.{js,ts,jsx,tsx,mdx}',
		'./components/**/*.{js,ts,jsx,tsx,mdx}',
		'./app/**/*.{js,ts,jsx,tsx,mdx}',
	],
	darkMode: 'class',
	plugins: [ require("daisyui") ],
	daisyui: {
		themes: [ { ...themes }, ...DEFAULT_DAISYUI_THEMES ],
	},
}
export default config
