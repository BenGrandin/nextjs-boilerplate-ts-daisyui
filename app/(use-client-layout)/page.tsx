import type { NextPage } from "next";
import { Button } from '@/components/atom/Button'

const Home: NextPage = () => {
	return (
		<section className='flex flex-row justify-start  flex-wrap gap-10'>
			<Button btnType='primary'>Primary</Button>
			<Button btnType='secondary'>Secondary</Button>
			<Button btnType='accent'>Accent</Button>
			<Button btnType='ghost'>Ghost</Button>
			<Button btnType='link'>Link</Button>
			<Button btnType='success'>Success</Button>
			<Button btnType='error'>Error</Button>
		</section>
	);
};

export default Home;
