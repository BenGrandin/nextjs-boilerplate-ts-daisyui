"use client"
import React, { CSSProperties, useEffect, useRef, useState } from 'react'
import Navbar from './Navbar'

export default function RootLayout({
									   children,
								   }: {
	children: React.ReactNode
}) {
	const navbarRef = useRef<HTMLElement>(null)
	const [ navHeight, setNavHeight ] = useState(navbarRef.current?.offsetHeight ?? 0)
	const mainStyle: CSSProperties = { minHeight: `calc(100vh - ${ navHeight }px)` }

	useEffect(() => {
		if (navbarRef.current?.offsetHeight) setNavHeight(navbarRef.current?.offsetHeight)
	}, [ navbarRef ]);

	return (
		<body>
		<Navbar ref={ navbarRef } />
		<main className='text-center py-10 px-5' style={ mainStyle }>
			{ children }
		</main>
		</body>
	)
}
