"use client"
import type { NextPage } from "next";
import DisplayAllThemesColors from '@/components/DisplayAllThemesColors'

const ShowCustomPalette: NextPage = () => {
	return (
		<>
			<div className='mb-8'>
				<h2>Custom themes</h2>
				<p>
					Here the list of all yours custom themes define in
					<span className='text-accent'> /themes </span> folder
				</p>
			</div>
			<DisplayAllThemesColors />
		</>
	);
};

export default ShowCustomPalette;
