import React, { forwardRef } from "react";
import Link from "next/link";
import ThemesDropdown from '@/components/ThemesDropdown'
import { usePathname } from 'next/navigation'
import { paths } from '@/app/paths'


const Navbar = forwardRef<HTMLElement>(function Navbar(_, ref) {
	const pathname = usePathname()

	return (
		<header ref={ ref } className='navbar sticky top-0 bg-base-200 z-1'>
			<div className='navbar-start'>
				<h1 className='ml-4 mb-0 text-xl'>DaisyUI Graphic Chart</h1>
			</div>
			<nav className='navbar-center'>
				<ul
					tabIndex={ 0 }
					className='flex rounded-box'
				>
					{ paths.map(({ name, href }) => (
						<li key={ name }>
							<Link
								className={ `btn px-8 font-medium ${ pathname == href ? "text-accent" : "" }` }
								href={ href }
							>
								{ name }
							</Link>
						</li>
					)) }
				</ul>
			</nav>
			<div className='navbar-end'>
				<ThemesDropdown />
			</div>
		</header>
	);
});

export default Navbar;
