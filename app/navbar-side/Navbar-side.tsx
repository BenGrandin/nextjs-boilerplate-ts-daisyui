import React from "react";
import Link from "next/link";
import { usePathname } from 'next/navigation'
import { paths } from '@/app/paths'

interface INavbarSideProps {
	isNavExpand: boolean
	toggle: (e: React.MouseEvent, value: boolean) => void
}


function NavbarSide({ isNavExpand, toggle }: INavbarSideProps) {
	const pathname = usePathname()

	const paddingX = isNavExpand ? 'p-4' : 'p-2'

	return (
		<header className={
			'navbar-side flex flex-col justify-between' +
			' content-center align-center text-center' +
			' text-sm bg-base-200' +
			' min-h-full pt-2 box-content' +
			' transition-all' +
			(isNavExpand ? ' w-64' : ' w-20 min-w-min')
		}>
			<h1 className={ `text-sm ${ paddingX }` }>DaisyUI Graphic Chart</h1>
			<nav className={ `navbar-medium ${ paddingX }` }>
				<ul tabIndex={ 0 }>
					{ paths.map(({ name, href }) => (
						<li key={ name } className='py-2'>
							<Link
								className={ `font-medium ${ pathname == href ? "text-accent" : "" }` }
								href={ href }
							>
								{ name }
							</Link>
						</li>
					)) }
				</ul>
			</nav>
			<div className='w-full center inline-flex p-2 bg-base-300 cursor-pointer'
				 onClick={ e => toggle(e, !isNavExpand) }>
				{ isNavExpand ? 'Collapse' : 'Expand' }
				{ isNavExpand
					? <svg xmlns='http://www.w3.org/2000/svg' fill='none' viewBox='0 0 24 24' strokeWidth={ 1.5 }
						   stroke='currentColor' className='w-6 h-6'>
						<path strokeLinecap='round' strokeLinejoin='round'
							  d='M18.75 19.5l-7.5-7.5 7.5-7.5m-6 15L5.25 12l7.5-7.5' />
					</svg>
					: <svg xmlns='http://www.w3.org/2000/svg' fill='none' viewBox='0 0 24 24' strokeWidth={ 1.5 }
						   stroke='currentColor' className='w-6 h-6'>
						<path strokeLinecap='round' strokeLinejoin='round'
							  d='M11.25 4.5l7.5 7.5-7.5 7.5m-6-15l7.5 7.5-7.5 7.5' />
					</svg>
				}

			</div>
		</header>
	);
}

export default NavbarSide;
