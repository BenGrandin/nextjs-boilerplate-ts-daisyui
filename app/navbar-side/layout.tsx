'use client'
import { ThemeContextProvider } from '@/app/themeContext'
import React, { useState } from 'react'
import NavbarSide from '@/app/navbar-side/Navbar-side'

const RootLayout = ({ children }: { children: React.ReactNode }) => {
	const [ isNavExpand, setIsNavExpand ] = useState(false)

	function toggle(e: React.MouseEvent, value: boolean) {
		if (value !== isNavExpand) {
			e.stopPropagation();
			setIsNavExpand(value)
		}
	}

	return (
		<ThemeContextProvider>
			<body className='flex'>
			<NavbarSide isNavExpand={ isNavExpand } toggle={ toggle } />

			<main className='flex-1 text-center py-10 px-5 '
				  onClick={ e => toggle(e, false) }>
				{ children }
			</main>
			</body>
		</ThemeContextProvider>
	)
}

export default RootLayout
