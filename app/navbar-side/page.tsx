// 'use client'
import React from 'react'
import Link from 'next/link'

const page = () => (
	<div className='page'>
		<h1>Hello</h1>

		<Link className='btn px-8 font-medium' href='/'>Go back home</Link>
	</div>
)

export default page
