'use client'
import React, { createContext, ReactNode, useEffect, useMemo, useState } from "react";

export interface ThemeContextProps {
	theme: string;
	setTheme: (theme: string) => void;
}

const DEFAULT_THEME = 'default'

export const ThemeContext = createContext<ThemeContextProps>({
	theme: DEFAULT_THEME,
	setTheme: () => {
	},
})

/**
 * Theme Context Provider
 *
 * @param value string
 * @param children ReactNode
 * @returns ReactNode
 */
export const ThemeContextProvider = ({
										 value,
										 children,
									 }: {
	value?: string,
	children: React.ReactNode,
}): ReactNode => {
	const [ theme, setTheme ] = useState(value ?? DEFAULT_THEME);
	const applyTheme = (theme: string) => {
		const html = document.getElementsByTagName("html")[0]; // Todo: Use body ?
		localStorage.setItem('theme', theme)
		html.setAttribute("data-theme", theme)
	}

	useEffect(() => {
		const storeTheme = localStorage.getItem("theme") ?? DEFAULT_THEME
		handleThemeChange(storeTheme)
	})

	const handleThemeChange = (theme: string) => {
		setTheme(theme);
		applyTheme(theme);
	};

	const val = useMemo(
		() => ({
			theme,
			setTheme: handleThemeChange,
			// eslint-disable-next-line react-hooks/exhaustive-deps
		}), [ theme ])

	return <ThemeContext.Provider value={ val }>
		{ children }
	</ThemeContext.Provider>
}
