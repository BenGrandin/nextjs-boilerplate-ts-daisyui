export enum PATHS {
	HOME = '/',
	CUSTOM_THEMES = '/custom-themes',
	THEME_PREVIEW = '/theme-preview',
	NAVBAR_SIDE = '/navbar-side',

}

export interface IPath {
	name: string
	href: string
}

export const paths: IPath[] = [
	{
		name: 'Homepage',
		href: PATHS.HOME,
	},
	{
		name: 'Custom themes',
		href: PATHS.CUSTOM_THEMES,
	},
	{
		name: 'Theme preview',
		href: PATHS.THEME_PREVIEW,
	},
	{
		name: 'Navbar side',
		href: PATHS.NAVBAR_SIDE,
	},
]
