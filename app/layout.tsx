"use client"
import './globals.css'
import React from 'react'
import { Inter } from 'next/font/google'
import { ThemeContextProvider } from '@/app/themeContext'

declare module 'react' {
	interface CSSProperties {
		[key: `--${ string }`]: string | number
	}
}
const inter = Inter({ subsets: [ 'latin' ] })

const RootLayout = ({
						children,
					}: {
	children: React.ReactNode
}) => (
	<html lang='en' id='app' className={ inter.className }>
	<head>
		<title>Graphic chart with daisyUI and Next.JS</title>
		<link rel='icon' href='./favicon.ico' />
		<meta charSet='utf-8' />
		<meta name='viewport' content='initial-scale=1.0, width=device-width' />
		<meta property='og:title' content='My page title' key='title' />
		<meta name='description' content='NextJS TS and DaysiUI Boilerplate' />
		<meta property='og:type' content='article' />
		<meta property='og:url' content='https://www.mysite.com/' />
		<meta property='og:image' content='https://www.site/icon.jpg' />
		<meta property='og:site_name' content='Webstie name' />
	</head>
	<ThemeContextProvider>
		{ children }
	</ThemeContextProvider>
	</html>
)
export default RootLayout
