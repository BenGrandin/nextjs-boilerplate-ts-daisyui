import Link from 'next/link'

const notFound = () => (
	<div className='min-h-full flex-col'>
		<h1 className='next-error-h1 border-r-accent text-xl pr-8 font-bold mt-8 mb-16'>Page not found</h1>
		<div>
			<h2 className='text-l mb-16'>Error 404: This page could not be found.</h2>
			<Link className='btn btn-wide btn-primary' href='/'>Return Home</Link>
		</div>
	</div>
)

export default notFound
